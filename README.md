# CMSR - Content Management System Repository. #

CMSR - Content Management System Repository.

## UNDER DEVELOPMENT ##

not for production use yet (dec 2021)

### What is this repository for? ###

Simple but trusted version control system for large filesystem oriented data banks. 

Not for source code but for binary data mostly. 

Should agregate functionality of RCS-like version manipulation and rsync-like transfer and synchronization 
with some subset of TIS-SQL approach to access control.

### development tool stack: ###

sh/awk for command line tools; java/servlet/jsp for production libraries, services and so on

### LICENSE: ###

BALES, MIT or BSD on your chois

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

UNDER CONSTRUCTION 
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

UNDER CONSTRUCTION 
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* cerrent public repo : git clone https://eustrop@bitbucket.org/eustrop/cmsr.git
* Ask for commerce support or join to development here [EustroSoft.org](http://eustrosoft.org)